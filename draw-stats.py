import argparse
from typing import TextIO

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def draw_stats(class_file: TextIO, out_img: str, graph_type: str, sim_col: str, class_col: str, title: str, csv_out: str | None):
    df = pd.read_csv(class_file)

    def df_count(dfx: pd.DataFrame) -> int:
        return len(dfx.index)

    threshold_lst = np.linspace(0, 1, 101)

    df_true_match = df[df[class_col] == 1]
    df_true_nonmatch = df[df[class_col] == 0]

    count_match = df_count(df_true_match)
    count_nonmatch = df_count(df_true_nonmatch)

    results_dict = {
        "threshold": [],
        "tp": [],
        "tn": [],
        "fp": [],
        "fn": [],
        "precision": [],
        "recall": [],
        "f1_score": [],
        "f_weight": [],
    }

    no_pp_warn = False

    for t in threshold_lst:
        # select all rows from the df with true matches that are above the threshold, yielding all true positives
        count_tp = df_count(df_true_match[df_true_match[sim_col] >= t])
        # select all rows from the df with true non-matches that are below the threshold, yielding all true negatives
        count_tn = df_count(df_true_nonmatch[df_true_nonmatch[sim_col] < t])
        # true matches = true positives + false negatives
        count_fn = count_match - count_tp
        # true non-matches = true negatives + false positives
        count_fp = count_nonmatch - count_tn

        if count_tp + count_fp == 0:  # no predicted positives
            if not no_pp_warn:
                no_pp_warn = True
                print("No predicted positives for some threshold values. Precision will be set to 1 for these "
                      "thresholds. This is done to make the output graph look neat, since the precision for no "
                      "predicted positives is undefined.")

            precision = 1
        else:
            precision = count_tp / (count_tp + count_fp)

        recall = count_tp / (count_tp + count_fn)
        f1_score = 2 * precision * recall / (precision + recall)
        f_weight = (count_fn + count_tp) / (count_fn + count_fp + 2 * count_tp)

        results_dict["threshold"].append(t)
        results_dict["tp"].append(count_tp)
        results_dict["tn"].append(count_tn)
        results_dict["fp"].append(count_fp)
        results_dict["fn"].append(count_fn)
        results_dict["precision"].append(precision)
        results_dict["recall"].append(recall)
        results_dict["f1_score"].append(f1_score)
        results_dict["f_weight"].append(f_weight)

    df_results = pd.DataFrame(data=results_dict)

    if graph_type == "performance":
        plt.rc("font", size=10)

        ax = df_results.plot.line(
            x="threshold",
            y=["precision", "recall", "f1_score"],
            grid=True,
            #    title=title
        )

        ax.set_xlim(1, 0)
        ax.annotate(f"True matches: {count_match}\n"
                    f"True non-matches: {count_nonmatch}\n"
                    f"Class imbalance: 1:{round(count_nonmatch / count_match, 2)}",
                    xy=(.98, .98),
                    xycoords="axes fraction",
                    horizontalalignment="right",
                    verticalalignment="top")
        ax.legend(["Precision", "Recall", "F1-Score"], loc="center right")
        ax.set_xlabel("Threshold")
        ax.set_xticks(np.linspace(0, 1, 11))
        ax.set_yticks(np.linspace(0, 1, 11))
        ax.get_figure().savefig(out_img)
    elif graph_type == "fscore":
        ax = df_results.plot.line(
            x="f_weight",
            y="f1_score",
            grid=True
        )

        ax.set_xlim(0, 1)
        ax.set_ylim(0, 1)
        ax.set_xlabel("p")
        ax.set_ylabel("F-Score")
        ax.get_legend().remove()

        ax.get_figure().savefig(out_img)

    if csv_out is not None:
        df_results.to_csv(csv_out)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("class-file", type=argparse.FileType("r", encoding="utf-8"),
                        help="file containing matches with annotated classes")
    parser.add_argument("out-img", type=str, help="path to write resulting graph to")
    parser.add_argument("graph-type", type=str, choices=["performance", "fscore"], help="type of graph to draw")
    parser.add_argument("--sim-col", type=str, default="similarity", help="column containing computed similarities")
    parser.add_argument("--class-col", type=str, default="is_match",
                        help="column containing boolean whether a row is a match or not")
    parser.add_argument("--title", type=str, default="Statistical measures", help="name of chart")
    parser.add_argument("--csv-out", type=str, help="path to raw csv output file")

    args = parser.parse_args()
    draw_stats(getattr(args, "class-file"), getattr(args, "out-img"), getattr(args, "graph-type"),
               args.sim_col, args.class_col, args.title, args.csv_out)
