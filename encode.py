import argparse
import csv
import datetime
import json
import time
from typing import TextIO

import requests


def encode(
        # file args
        attr_file: TextIO, record_file: TextIO, result_file: TextIO,
        # encoder args
        filter_type: str, strategy: str, seed: int, hashes: int, tokens: int, salt: str, key: str, hash_fn: str,
        # io/net args
        id_col: str, transform_url: str, mask_url: str, chunk_size: int
):
    schema_data = json.load(attr_file)
    attr_to_weight: dict[str, float] = {s["name"]: s["weight"] for s in schema_data}

    record_data = csv.DictReader(record_file)
    record_lst: list[dict] = []

    # running id in case none has been provided on the command line
    i = 0

    for r in record_data:
        i += 1
        r_id = str(i)

        # check if the record contains a custom id
        if id_col in r:
            r_id = str(r[id_col]).strip()
            # remove id from the record since we iterate over its attributes later
            del r[id_col]

        # create an empty copy of the record
        r_cpy = {"id": r_id}

        for a in r:
            # check that the attribute has a schema assigned to it. if not, ignore it.
            if a not in attr_to_weight:
                continue

            r_cpy[a] = r[a]

        record_lst.append(r_cpy)

    total_count = len(record_lst)
    transformed_record_lst: list[dict] = []

    print("starting transformation")
    start_time = time.monotonic()

    for i in range(0, total_count, chunk_size):
        record_lst_chunk = record_lst[i:i + chunk_size]

        resp = requests.post(transform_url, json={
            "entities": record_lst_chunk,
            "globalTransformers": [
                {
                    "name": "norm",
                    "order": "before"
                }
            ]
        }).json()

        transformed_record_lst.extend(resp["entities"])

        current_count = i + len(record_lst_chunk)
        print(f"progress: {round(100 * current_count / total_count, 2)}% ({current_count}/{total_count})")

        # time_passed / processed_records = expected_total_time / total_records, therefore:
        # expected_total_time = total_records * time_passed / processed_records
        time_diff_secs = time.monotonic() - start_time
        eta_time_secs = time_diff_secs * total_count / current_count
        # the remaining time is the total expected time minus the amount of time that has already passed
        eta_time_secs_left = eta_time_secs - time_diff_secs
        eta_time_delta = datetime.timedelta(seconds=eta_time_secs_left)

        print(f"eta: {str(eta_time_delta)}")

    print("starting masking")
    start_time = time.monotonic()

    w_results = csv.DictWriter(result_file, fieldnames=["id", "value"])
    w_results.writeheader()

    for i in range(0, total_count, chunk_size):
        record_lst_chunk = transformed_record_lst[i:i + chunk_size]

        resp = requests.post(mask_url, json={
            "entities": record_lst_chunk,
            "masking": {
                "tokenSize": tokens,
                "hashValues": hashes,
                "hashFunction": hash_fn,
                "key": key,
                "seed": seed,
                "hashStrategy": strategy,
                "filterType": filter_type.upper(),
                "salt": salt,
                "hardeners": [
                    {
                        "name": "balance"
                    },
                    {
                        "name": "permute"
                    }
                ]
            },
            "attributes": schema_data
        }).json()

        w_results.writerows(resp["entities"])

        current_count = i + len(record_lst_chunk)
        print(f"progress: {round(100 * current_count / total_count, 2)}% ({current_count}/{total_count})")

        # time_passed / processed_records = expected_total_time / total_records, therefore:
        # expected_total_time = total_records * time_passed / processed_records
        time_diff_secs = time.monotonic() - start_time
        eta_time_secs = time_diff_secs * total_count / current_count
        # the remaining time is the total expected time minus the amount of time that has already passed
        eta_time_secs_left = eta_time_secs - time_diff_secs
        eta_time_delta = datetime.timedelta(seconds=eta_time_secs_left)

        print(f"eta: {str(eta_time_delta)}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("attrs", type=argparse.FileType("r", encoding="utf-8"), help="file containing schema "
                                                                                     "definitions")
    parser.add_argument("records", type=argparse.FileType("r", encoding="utf-8"), help="file containing records")
    parser.add_argument("results", type=argparse.FileType("w", encoding="utf-8"),
                        help="file to write encoded records to")

    parser.add_argument("--filter", type=str, choices=["rbf", "clkrbf"], default="clkrbf", help="filter type to use")
    #parser.add_argument("--strategy", type=str, default="RANDOM_SHA256", help="hash strategy to use")
    parser.add_argument("--strategy", type=str, default="randomHash", choices=["doubleHash", "enhancedDoubleHash", "randomHash"])
    parser.add_argument("--hashfn", type=str, default="HMAC-SHA256", help="hash function to use")
    parser.add_argument("--seed", type=int, default=0, help="seed to apply to random permutations")
    parser.add_argument("--salt", type=str, default="", help="salt to apply to tokens")
    parser.add_argument("--id", type=str, default="id", help="name of ID column")
    parser.add_argument("-k", "--hashes", type=int, default=5, help="hash value count")
    parser.add_argument("-q", "--tokens", type=int, default=2, help="token size")
    parser.add_argument("--key", type=str, default=None, help="key to use for HMAC-based strategies")
    parser.add_argument("--transform", type=str, default="http://localhost:8080/transform", help="transform endpoint")
    parser.add_argument("--mask", type=str, default="http://localhost:8080/mask", help="mask endpoint")
    parser.add_argument("--chunk", type=int, default=1000, help="chunk size")

    args = parser.parse_args()

    encode(args.attrs, args.records, args.results, args.filter, args.strategy, args.seed, args.hashes,
           args.tokens, args.salt, args.key, args.hashfn, args.id, args.transform, args.mask, args.chunk)
