import argparse
import csv
from typing import TextIO


def classify_matches(match_info_file: TextIO, match_info_id_cols: tuple[str, str], match_file: TextIO,
                     match_id_cols: tuple[str, str], out_file: TextIO, sim_col: str):
    match_info_csv = csv.DictReader(match_info_file)
    # set of all id pairs that make up the true matches
    match_pair_set: set[tuple[str, str]] = set()
    d_col, r_col = match_info_id_cols

    for line in match_info_csv:
        if d_col not in line or r_col not in line:
            raise ValueError(f"match info file is missing either '{d_col}' or '{r_col}' column")

        match_pair_set.add((line[d_col], line[r_col]))

    match_csv = csv.DictReader(match_file)
    d_col, r_col = match_id_cols

    w_out = csv.DictWriter(out_file, fieldnames=[d_col, r_col, sim_col, "is_match"])
    w_out.writeheader()

    for line in match_csv:
        if d_col not in line or r_col not in line:
            raise ValueError(f"result file is missing either '{d_col}' or '{r_col}' column")

        line_domain = line[d_col]
        line_range = line[r_col]

        # check if the combination of domain and range id appears in the set of known true matches
        is_match = (line_domain, line_range) in match_pair_set or (line_range, line_domain) in match_pair_set
        w_out.writerow({
            d_col: line_domain,
            r_col: line_range,
            sim_col: line[sim_col],
            "is_match": 1 if is_match else 0
        })


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("match-info-file", type=argparse.FileType("r", encoding="utf-8"),
                        help="file containing info about true match pairings")
    parser.add_argument("match-info-id-cols", type=str,
                        help="name of ID pair columns in match info file, separated by comma")
    parser.add_argument("match-file", type=argparse.FileType("r", encoding="utf-8"),
                        help="file containing matches with computed similarities")
    parser.add_argument("match-id-cols", type=str, help="name of ID pair columns in match file, separated by comma")
    parser.add_argument("out-file", type=argparse.FileType("w", encoding="utf-8"),
                        help="file to write matches with class to")
    parser.add_argument("--similarity-col", type=str, default="similarity",
                        help="column in match file containing similarity info")


    def split_id_pair(s: str) -> tuple[str, str]:
        x = s.split(",")

        if len(x) != 2:
            raise ValueError(f"'{s}' is not a pair")

        return x[0], x[1]


    args = parser.parse_args()

    match_info_id_cols = split_id_pair(getattr(args, "match-info-id-cols"))
    match_id_cols = split_id_pair(getattr(args, "match-id-cols"))

    # classify_matches(args.base, base_id_pair, args.results, result_id_pair, args.out)
    classify_matches(getattr(args, "match-info-file"),
                     match_info_id_cols,
                     getattr(args, "match-file"),
                     match_id_cols,
                     getattr(args, "out-file"),
                     args.similarity_col)
