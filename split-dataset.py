import argparse
import os.path
import random
from typing import TextIO


def split_dataset(in_file: TextIO, out_file: str, count: int, split_file: TextIO, seed: int, has_header: bool):
    record_dict: dict[str, list[str]] = {}
    header_line: str | None = None

    if has_header:
        header_line = next(in_file)

    for line in in_file:
        # ids are formatted as "rec-XXX-org/dup", followed by the rest of the line
        rec_id = line.split("-")[1]

        # add list if id doesn't exist yet
        if rec_id not in record_dict:
            record_dict[rec_id] = []

        record_dict[rec_id].append(line)

    # if out_file is "foo.csv", then split name and ext so that we can create foo-1.csv, foo-2.csv and so on
    out_file_name, out_file_ext = os.path.splitext(out_file)
    out_files: list[TextIO] = []

    for i in range(count):
        out_files.append(open(out_file_name + "-" + str(i + 1) + out_file_ext, mode="w", encoding="utf-8"))

        if has_header:
            out_files[i].write(header_line)

    random.seed(seed)
    split_file.write("id1,id2\n")

    for rec_id, record_list in record_dict.items():
        # list of file indices
        file_idx_list = [i for i in range(count)]
        random.shuffle(file_idx_list)

        # keep track of records that were written
        written_rec_ids: list[str] = []

        # it's possible for a record to have fewer duplicates than there are files to write to
        for i in range(min(len(record_list), count)):
            # write to output file
            out_files[file_idx_list[i]].write(record_list[i])
            # remember the full record id
            written_rec_ids.append(record_list[i].split(",")[0])

        # it's possible for a record to have no duplicates
        if len(written_rec_ids) > 1:
            # let the list be [001-org, 001-dup-1, 001-dup-2]. goal: "cross product" without self-matches.
            # 1st iter) outer loop: 001-org. inner loop: 001-dup-1, 001-dup-2.
            # 2nd iter) outer loop: 001-dup-1. inner loop: 001-dup-2.
            for x in range(len(written_rec_ids) - 1):
                for y in range(x + 1, len(written_rec_ids)):
                    split_file.write(",".join([written_rec_ids[x], written_rec_ids[y]]) + "\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("in-file", type=argparse.FileType(mode="r", encoding="utf-8"))
    parser.add_argument("out-file", type=str)
    parser.add_argument("count", type=int)
    parser.add_argument("split-file", type=argparse.FileType(mode="w", encoding="utf-8"))
    parser.add_argument("--seed", type=int, default=42)
    parser.add_argument("--has-header", action="store_true")

    args = parser.parse_args()

    split_dataset(getattr(args, "in-file"), getattr(args, "out-file"), args.count, getattr(args, "split-file"),
                  args.seed, args.has_header)
