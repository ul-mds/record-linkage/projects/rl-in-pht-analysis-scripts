import argparse
import csv
import datetime
import time
from typing import TextIO

import requests


def match(
        # file args
        domain_file: TextIO, range_file: TextIO, result_file: TextIO,
        # matcher args
        measure: str, threshold: float,
        # io/net args
        chunk_size: int, matcher_url: str
):
    total_matches = 0

    domain_data = csv.DictReader(domain_file)
    range_data = csv.DictReader(range_file)

    domain_dict_lst = [{"id": d["id"], "value": d["value"]} for d in domain_data]
    range_dict_lst = [{"id": r["id"], "value": r["value"]} for r in range_data]

    print(f"len(domain)={len(domain_dict_lst)}")
    print(f"len(range)={len(range_dict_lst)}")

    target_count = len(domain_dict_lst) * len(range_dict_lst)
    print(f"comparisons to perform: {target_count}")

    current_count = 0

    w_results = csv.DictWriter(result_file, fieldnames=["domain_id", "range_id", "similarity"])
    w_results.writeheader()

    i = 0  # outer loop (domain)

    start_time = time.monotonic()

    while True:
        # get next domain vector chunk
        d_lst = domain_dict_lst[i:i + chunk_size]

        # exit condition
        if len(d_lst) == 0:
            break

        j = 0  # inner loop (range)

        while True:
            # get next range vector chunk
            r_lst = range_dict_lst[j:j + chunk_size]

            # exit condition
            if len(r_lst) == 0:
                break

            resp = requests.post(matcher_url, json={
                "config": {
                    "measure": measure,
                    "threshold": threshold
                },
                "domain": d_lst,
                "range": r_lst
            }).json()

            matches = resp["matches"]

            # update match count
            current_count += len(d_lst) * len(r_lst)
            total_matches += len(matches)

            print(f"progress: {round(100 * current_count / target_count, 2)}% ({current_count}/{target_count})")

            # see encode script on why this works
            time_diff_secs = time.monotonic() - start_time
            eta_time_secs = time_diff_secs * target_count / current_count
            eta_time_secs_left = eta_time_secs - time_diff_secs
            eta_time_delta = datetime.timedelta(seconds=eta_time_secs_left)

            print(f"eta: {str(eta_time_delta)}")
            print(f"{total_matches} match{'es' if total_matches != 1 else ''} identified")

            # store results
            for m in matches:
                d = m["domain"]["id"]
                r = m["range"]["id"]
                c = m["similarity"]

                w_results.writerow({
                    "domain_id": d,
                    "range_id": r,
                    "similarity": c
                })

            # increment loop
            j += chunk_size

        # increment loop
        i += chunk_size


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("domain", type=argparse.FileType("r", encoding="utf-8"), help="file containing domain records")
    parser.add_argument("range", type=argparse.FileType("r", encoding="utf-8"), help="file containing range records")
    parser.add_argument("result", type=argparse.FileType("w", encoding="utf-8"), help="file to write results to")

    parser.add_argument("--measure", type=str, choices=["dice", "jaccard", "cosine"], default="jaccard",
                        help="similarity measure to use")
    parser.add_argument("-c", "--chunk", type=int, default=100, help="chunk size")
    parser.add_argument("-t", "--threshold", type=float, default=0, help="threshold")

    parser.add_argument("--matcher", type=str, default="http://localhost:8080/match", help="matcher endpoint")

    args = parser.parse_args()
    match(args.domain, args.range, args.result, args.measure, args.threshold, args.chunk, args.matcher)
