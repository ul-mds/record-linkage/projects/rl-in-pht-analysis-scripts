import argparse
import json
import math
import os
from typing import TextIO, NamedTuple


class WordlistStats(NamedTuple):
    average_tokens: float
    weight: float


def sanitize(s: str) -> str:
    s = s.lower().strip()
    s_new = ""

    for c in s:
        match c:
            case 'ä':
                s_new += "ae"
            case 'ö':
                s_new += "oe"
            case 'ü':
                s_new += "ue"
            case 'ß':
                s_new += "ss"
            case '-':
                continue
            case _:
                s_new += c

    return s_new


def tokenize(s: str, q: int) -> set[str]:
    tokens: set[str] = set()

    padding = " " * (q - 1)
    s = padding + s + padding

    for i in range(len(s) - q + 1):
        tokens.add(s[i:i + q])

    return tokens


def compute_wordlist_stats(in_file: TextIO, q: int) -> WordlistStats:
    average_tokens = 0
    total_word_count = 0

    token_to_count_dict: dict[str, int] = {}

    for row in in_file:
        # sanitize and tokenize value
        tokens = tokenize(sanitize(str(row)), q)
        # update token count
        average_tokens += len(tokens)

        for token in tokens:
            # initialize token if it's not present in the dict yet
            if token not in token_to_count_dict:
                token_to_count_dict[token] = 0

            # increment total count of token by one
            token_to_count_dict[token] += 1

        total_word_count += 1

    # right now average_tokens still holds the total amount of tokens across
    # the entire list. save it because we need it later.
    total_token_count = average_tokens
    token_entropy = 0

    for token_count in token_to_count_dict.values():
        p = token_count / total_token_count
        token_entropy += p * math.log2(p)

    token_entropy = -token_entropy
    average_tokens /= total_word_count

    return WordlistStats(average_tokens, token_entropy)


def generate_attributes_json(wordlist_dir: str, out_file: TextIO, q: int):
    attribute_json = []

    for file_name in os.listdir(wordlist_dir):
        attr_name, _ = os.path.splitext(file_name)
        file_path = os.path.join(wordlist_dir, file_name)

        with open(file_path, mode="r", encoding="utf-8") as f:
            stats = compute_wordlist_stats(f, q)

        attribute_json.append({
            "name": attr_name,
            #"type": "string",
            "averageTokenCount": stats.average_tokens,
            "weight": stats.weight
        })

    json.dump(attribute_json, out_file, indent=2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("wordlist-dir", type=str, help="directory containing wordlists")
    parser.add_argument("out-file", type=argparse.FileType(mode="w", encoding="utf-8"), help="file to write results to")
    parser.add_argument("-q", type=int, default=2, help="token size")

    args = parser.parse_args()
    generate_attributes_json(getattr(args, "wordlist-dir"), getattr(args, "out-file"), args.q)

    print("Done! You may need to adjust the attribute options, if necessary.")
