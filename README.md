# PPRL in PHT paper code

## Setup

This project requires Python 3, Docker and Docker Compose.
In the root directory of this project, create a new virtual environment using `python -m venv ./venv`.
Activate the virtual environment with `source ./venv/bin/activate`.
Install project dependencies with `python -m pip install -r requirements.txt`.
Change into the [deployment directory](./deploy) and run `docker compose up -d`.
This is necessary if you intend to use the encoding and matching scripts.

## Commands used for the paper

First, prepare the data directory structure.

```
mkdir -p data/fake/lst
mkdir -p data/fake/masked
mkdir -p data/fake/original
mkdir -p data/fake/matched
```

Store your original input data in `data/fake/original.csv`.
Reference data is generated to create an attribute schema file.

```
faker -l de_DE -o data/fake/lst/given_name.lst -r 1000 -s "" --seed 42 first_name
faker -l de_DE -o data/fake/lst/last_name.lst -r 1000 -s "" --seed 42 last_name
faker -l de_DE -o data/fake/lst/birth_date.lst -r 1000 -s "" --seed 42 date
faker -l de_DE -o data/fake/lst/gender.lst -r 1000 -s "" --seed 42 random_element mf
faker -l de_DE -o data/fake/lst/city.lst -r 1000 -s "" --seed 42 city
python generate-attributes-json.py data/fake/lst data/fake/attributes-q2.json -q 2
```

First, the original dataset is split into three smaller datasets.
Duplicates can only be found between files.

```
python split-dataset.py data/fake/original.csv data/fake/original/pprl-data.csv 3 data/fake/split-info.csv --has-header
```

Next, the individual files are encoded and masked.
The following line applies the following techniques.

- CLKRBF with average token count and attribute weights sourced from an external file
- Base hash value count of five (adapted on per-attribute basis with CLKRBF)
- Random hashing with keyed SHA256 as the backing hash function
- Tokenization into text fragments of size two
- Seed for RNG-based operations
- Global attribute salting

```
python encode.py data/fake/attributes-q2.json data/fake/original/pprl-data-1.csv data/fake/masked/pprl-data-1.csv --filter clkrbf --strategy randomHash --salt s4ltY --id id --key s3cr3t -k 5 -q 2 --seed 42 --chunk 100
```

The masked vectors are matched against one another.
The following line used the Jaccard index as the similarity measure.
Thresholding is not applied.

```
python match.py data/fake/masked/pprl-data-1.csv data/fake/masked/pprl-data-2.csv data/fake/matched/pprl-data-1-2-match.csv --measure jaccard -c 100 -t 0
```

The next block merges all files with match information into one.

```
cd data/fake/matched
tail -n +2 pprl-data-1-3-match.csv >> pprl-data-1-2-match.csv
tail -n +2 pprl-data-2-3-match.csv >> pprl-data-1-2-match.csv
mv pprl-data-1-2-match.csv pprl-data-match.csv
rm pprl-data-*-match.csv
cd ../../..
```

The matches are then merged with the information about known true matches.

```
python classify-matches.py data/fake/split-info.csv id1,id2 data/fake/matched/pprl-data-match.csv domain_id,range_id data/fake/pprl-data-class.csv
```

A performance graph can then be generated using the following line.

```
python draw-stats.py data/fake/pprl-data-class.csv data/fake/output.pdf performance
```